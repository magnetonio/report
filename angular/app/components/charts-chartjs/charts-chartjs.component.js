class ChartsChartjsController {
  constructor ($scope, API) {
    'ngInject'

    this.API = API
    this.data = {};

    let Datasources = this.API.service('datasources', this.API.all('users'))

    //Get datasources
    Datasources.getList()
      .then((response) => {
        this.datasources = response.plain()        
      })
  }                         

  initData () {
    if (this.primary_field){
      var dataSrc = this.chartData
      this.chart = {}

      for(var column_index = 0; column_index < this.providers.length; column_index++){
        var column_id = this.providers[column_index]['column_id']
        this.chart[column_id] = []

        for ( var data_index = 0; data_index < dataSrc.length; data_index++){
          this.chart[column_id].push(dataSrc[data_index][column_id])
        }
      }
    }
  }

  //Get providers with selected datasource
  changeDatasource (){
    let Dataprovider = this.API.service('dataprovider', this.API.all('users'))
    
    if (this.current_datasource == null) return

    Dataprovider.getList({
      'datasource': this.current_datasource
    }).then((response) => {
      let dataSet = response.plain()

      if (dataSet.length == 0) return
      this.current_sql = dataSet[0]['sql']
      this.totalCount = dataSet[0]['totalCount']
      this.dataSet = dataSet[0]['providers']
      this.providers = this.dataSet
    })
  }

  //Set chart data
  changePrimaryField () {
    if (this.primary_field){
      this.lineChartData = []
      this.lineChartSeries = []

      this.areaChartData = []
      this.areaChartSeries = []

      this.barChartData = []
      this.barChartSeries = []

      if (this.chart == null) return
      this.lineChartLabels = this.chart[this.primary_field]
      this.areaChartLabels = this.chart[this.primary_field]
      this.barChartLabels = this.chart[this.primary_field]

      //Generating chart data
      for (var index = 0; index < this.providers.length; index++){
        var column_id = this.providers[index]['column_id']

        if (this.providers[index]['column_id'] != this.primary_field){
          //line chart
          this.lineChartData.push(this.chart[column_id])
          this.lineChartSeries.push(this.providers[index]['column_id'])

          //area chart
          this.areaChartData.push(this.chart[column_id])
          this.areaChartSeries.push(this.providers[index]['column_id'])

          //bar chart
          this.barChartData.push(this.chart[column_id])
          this.barChartSeries.push(this.providers[index]['column_id'])

        }
      }
    }
  }

  //start_from, length change event handlers
  changeFilterStart (){
    this.loadDatafeed()
  }
  changeFeedLength(){
    this.loadDatafeed()
  }
  loadDatafeed () {
    let ChartData = this.API.service('chartdata', this.API.all('users'))
    if (this.start_from == null) this.start_from = 0
    if (this.feed_length == null) return;
    ChartData.getList({
      'datasource': this.current_datasource,
      'start': this.start_from,
      'length': this.feed_length
    }).then((response) => {
      this.dtColumns = []                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
      let dataSet = response.plain()

      if (dataSet.length == 0) return

      this.chartData = dataSet
      this.initData()
      this.changePrimaryField()

    })
  }
  $onInit () {

    /*this.lineChartLabels = this.data['tid']
    this.lineChartSeries = ['Series A', 'Series B']
    this.lineChartData = [
      this.data['fmt-load-amount'],
      this.data['fmt-load-price']
    ]*/
    
    /*this.areaChartLabels = ['Januarys', 'February', 'March', 'April', 'May', 'June', 'July']
    this.areaChartSeries = ['Series A', 'Series B']
    this.areaChartData = [
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
    ]
    */
    this.areaChartColours = [
      {
        fillColor: '#D2D6DE',
        strokeColor: '#D2D6DE',
        pointColor: 'rgba(148,159,177,1)',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(148,159,177,0.8)'
      },
      {
        fillColor: '#4B94C0',
        strokeColor: '#4B94C0',
        pointColor: '#2980b9',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(77,83,96,1)'
      }
    ]

    this.onClick = function () {}
    /*
    this.barChartLabels = ['Januarys', 'February', 'March', 'April', 'May', 'June', 'July']
    //this.barChartSeries = ['Series A', 'Series B', 'Series C']
    this.barChartData = [
      [28, 48, 40, 19, 86, 27, 90],
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
    ]
    */
    this.barChartColours = [
      {
        fillColor: '#D2D6DE',
        strokeColor: '#D2D6DE',
        pointColor: 'rgba(148,159,177,1)',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(148,159,177,0.8)'
      },
      {
        fillColor: '#00A65A',
        strokeColor: '#00A65A',
        pointColor: '#2980b9',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(77,83,96,1)'
      }
    ]

    this.pieLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales']
    this.pieData = [300, 500, 100]
  }
}

export const ChartsChartjsComponent = {
  templateUrl: './views/app/components/charts-chartjs/charts-chartjs.component.html',
  controller: ChartsChartjsController,
  controllerAs: 'vm',
  bindings: {}
}
