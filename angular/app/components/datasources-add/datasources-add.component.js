class DatasourcesAddController {
  constructor (API, $scope, $http, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []
    this.$scope = $scope
    this.$http = $http
    
    if (this.driver == 'csv'){
      this.isDBDriver = false
    } else {
      this.isDBDriver = true
    }

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }

  changeDriver (){
    if (this.driver == 'csv'){
      this.isDBDriver = false
    } else {
      this.isDBDriver = true
    }
  }

  save (isValid) {
    if (!this.isDBDriver){
      var f = document.getElementById('csvfile').files[0]
      var r = new FileReader();
      let $http = this.$http

      r.onloadend = function(e) {
          var data = e.target.result
          console.log(data)
          var fd = new FormData()
          fd.append('file', data)
          fd.append('file_name', f.name)

          $http.post('/api/users/datasources', fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
          })
          .success(function(){
              console.log('success')
          })
          .error(function(){
              console.log('error')
          })
      }

      r.readAsDataURL(f)
    }else{
      this.$state.go(this.$state.current, {}, { alerts: 'test' })
      if (isValid) {
        let Datasources = this.API.service('datasources', this.API.all('users'))
        let $state = this.$state

        Datasources.post({
          'driver': this.driver,
          'db_name': this.db_name,
          'alias': this.alias,
          'username': this.username,
          'password': this.password,
          'port': this.port
        }).then(function () {
          let alert = { type: 'success', 'title': 'Success!', msg: 'Datasource has been added.' }
          $state.go($state.current, { alerts: alert})
        }, function (response) {
          let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
          $state.go($state.current, { alerts: alert})
        })
      } else {
        this.formSubmitted = true
      }
    }
  }

  $onInit () {}
}

export const DatasourcesAddComponent = {
  templateUrl: './views/app/components/datasources-add/datasources-add.component.html',
  controller: DatasourcesAddController,
  controllerAs: 'vm',
  bindings: {}
}
