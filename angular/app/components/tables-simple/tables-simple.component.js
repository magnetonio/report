class TablesSimpleController {
  constructor ($rootScope, $window, $scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
    'ngInject'
    this.API = API
    this.$state = $state
    this.$window = $window
    this.$rootScope = $rootScope
    this.formSubmitted = false
    this.alerts = []
    this.DTOptionsBuilder = DTOptionsBuilder
    this.DTColumnBuilder = DTColumnBuilder
    this.$compile = $compile
    this.$scope = $scope

    let Datasources = this.API.service('datasources', this.API.all('users'))

    //Get datasources
    Datasources.getList()
      .then((response) => {
        this.datasources = response.plain()        
      })
  }

  //Get providers with selected datasource
  changeDatasource (){
    let Dataprovider = this.API.service('dataprovider', this.API.all('users'))
    let DTColumnBuilder = this.DTColumnBuilder
    let DTOptionsBuilder = this.DTOptionsBuilder
    let $window = this.$window

    if (this.current_datasource == null) return

    Dataprovider.getList({
      'datasource': this.current_datasource
    }).then((response) => {
      let dataSet = response.plain()

      if (dataSet.length == 0) return
      this.current_sql = dataSet[0]['sql']
      this.totalCount = dataSet[0]['totalCount']
      this.dataSet = dataSet[0]['providers']
      this.providers = this.dataSet
      this.dtColumns = []

      for (var index = 0; index < this.providers.length; index++){
       this.dtColumns.push(DTColumnBuilder.newColumn(this.providers[index]['column_id']).withTitle(this.providers[index]['title']))
      }

      this.dtOptions = null
      this.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
          dataSrc: function(data){
            return data.data
          },
          url: "/api/users/datafeed?datasource=" + this.current_datasource,
          data: function (d) {
          },
          type:"POST",
          beforeSend: function(xhr){
            xhr.setRequestHeader("Authorization", 
              'Bearer ' + $window.localStorage.satellizer_token)
          }
        })
        .withOption('processing', true) //for show progress bar
        .withOption('serverSide', true) // for server side processing
        .withPaginationType('full_numbers') // for get full pagination options // first / last / prev / next and page numbers
        .withDisplayLength(10) // Page size
        .withPaginationType('simple_numbers')
        .withOption('responsive', true)
        .withOption('aaSorting',[0,'asc']) // for default sorting column // here 0 means first column

      this.displayTable = true
    })
  }
  $onInit () {}

}

export const TablesSimpleComponent = {
  templateUrl: './views/app/components/tables-simple/tables-simple.component.html',
  controller: TablesSimpleController,
  controllerAs: 'vm',
  bindings: {}
}
